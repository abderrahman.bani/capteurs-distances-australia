import numpy.random
import pandas
import folium
import folium.plugins
import requests
import polyline
import json
from numpy import random
from codetiming import Timer


# read data
coords_capteurs = pandas.read_csv("coords_capteurs.csv", sep=",", decimal=".")
Distances = json.load(open("./Distances.json", 'r', encoding="utf-8"))


# save distances in json file
def save_distances_file():
    with open("./Distances.json", 'w', encoding="utf-8") as f:
        json.dump(Distances, f, indent=2)

#  find walking route between from a to b


def get_route(start_capteur, destination_capteur):
    loc = "{},{};{},{}?overview=false".format(start_capteur["longitude"],
                               start_capteur["latitude"],
                               destination_capteur["longitude"],
                               destination_capteur["latitude"])
    url = "http://127.0.0.1:5000/route/v1/walking/" # walking
	key =  start_capteur["Capt_ID"]+";"+destination_capteur["Capt_ID"]
	if key not in Distance:
		try:
			r = requests.get(url + loc)
			if r.status_code != 200:
				print("prob in", url + loc)
				return {}
			res = r.json()
			out = {'Start_Capt_ID': start_capteur["Capt_ID"],
				   'Destination_Capt_ID': destination_capteur["Capt_ID"],
				   'Walking Distance (m)': res['routes'][0]['distance'],
				   'Walking Duration (s)': res['routes'][0]['duration']
				   }
			Distance[key]=out
		except:
			print("An exception occurred, Save distance file")
			save_distances_file()
    return Distance[key]


## distances calculation
df_distances = []
#coords_capteurs = coords_capteurs.head(5)

t = Timer()
t.start()
t2 = Timer()
coords_capteurs = coords_capteurs.T.to_dict().values()
for i, start_capteur in enumerate(coords_capteurs):
    print(start_capteur['Capt_ID'])
    t2.start()
    for j, destination_capteur in enumerate(coords_capteurs):
        if i < j:
            df_distances.append(get_route(start_capteur, destination_capteur))
	save_distances_file()
    t2.stop()
t.stop()
# save distances dataframe
distances_dataframe = pandas.DataFrame.from_records(df_distances)
distances_dataframe[['Start_Capt_ID', 'Destination_Capt_ID', 'Walking Distance (m)',
                    'Walking Duration (s)']].to_csv("./Distances.csv", index=False)


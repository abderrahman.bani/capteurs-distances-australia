# OSRM walking distances calculation
![Screenshot](Carte.PNG)
# install  requests and polyline and codetiming:
    pip install requests polyline codetiming
# Install OSRM-BACKEND from https://github.com/Project-OSRM/osrm-backend

We base our Docker images ([backend](https://hub.docker.com/r/osrm/osrm-backend/), [frontend](https://hub.docker.com/r/osrm/osrm-frontend/)) on Debian and make sure they are as lightweight as possible.

Download OpenStreetMap extracts for example from [Geofabrik](http://download.geofabrik.de/)

    wget http://download.geofabrik.de/australia-oceania/australia-latest.osm.pbf

Pre-process the extract with the car profile and start a routing engine HTTP server on port 5000

    docker run -t -v "${PWD}:/data" osrm/osrm-backend osrm-extract -p /opt/foot.lua /data/australia-latest.osm.pbf

The flag `-v "${PWD}:/data"` creates the directory `/data` inside the docker container and makes the current working directory `"${PWD}"` available there. The file `/data/canada-latest.osm.pbf` inside the container is referring to `"${PWD}/canada-latest.osm.pbf"` on the host.

    docker run -t -v "${PWD}:/data" osrm/osrm-backend osrm-partition /data/australia-latest.osrm
    docker run -t -v "${PWD}:/data" osrm/osrm-backend osrm-customize /data/australia-latest.osrm

Note that `australia-latest.osrm` has a different file extension. 

    docker run -t -i -p 5000:5000 -v "${PWD}:/data" osrm/osrm-backend osrm-routed --algorithm mld /data/australia-latest.osrm

Make requests against the HTTP server

    curl "http://127.0.0.1:5000/route/v1/walking/144.9536,-37.81686;144.9615,-37.81235"

Optionally start a user-friendly frontend on port 9966, and open it up in your browser

    docker run -p 9966:9966 osrm/osrm-frontend
    xdg-open 'http://127.0.0.1:9966'



